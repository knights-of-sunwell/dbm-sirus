local mod = DBM:NewMod("MagicDevourer", "DBM-TolGarodPrison")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4842 $"):sub(12, -3))
mod:SetCreatureID(18481)
mod:RegisterCombat("combat")

mod:RegisterEvents(
    "SPELL_CAST_START",
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_REMOVED",
    "SPELL_DAMAGE"
)

local warnFelActivation      = mod:NewSpellAnnounce(317653, 3)
local warnFelOverload        = mod:NewTargetAnnounce(317666, 2)
local warnDarkActivation     = mod:NewSpellAnnounce(317650, 3)
local warnDarkOverload       = mod:NewTargetAnnounce(317662, 2)
local warnBarrageSoon        = mod:NewSoonAnnounce(317685, 3)
local warnStaggeringBlow     = mod:NewSpellAnnounce(317673, 3, nil, mod:IsMelee())
local warnMagicFel           = mod:NewTargetAnnounce(317681, 1, nil, mod:IsTank())
local warnMagicDark          = mod:NewTargetAnnounce(317683, 1, nil, mod:IsTank())

local specWarnFelOverload    = mod:NewSpecialWarningYou(317666)
local specWarnSpillingFel    = mod:NewSpecialWarningSpell(317645)
local specWarnDarkOverload   = mod:NewSpecialWarningYou(317662)
local specWarnEnvelopingDark = mod:NewSpecialWarningSpell(317641)
local specWarnBarrage        = mod:NewSpecialWarningSpell(317685)
local specWarnShadows        = mod:NewSpecialWarningCast(317674, mod:IsRanged() and not mod:IsPhysical())

local yellFel                = mod:NewYell(317666)
local yellDark               = mod:NewYell(317662)

local timerFelCD             = mod:NewCDTimer(44, 317653)
local timerFelOverload       = mod:NewBuffActiveTimer(8, 317653)
local timerDarkCD            = mod:NewCDTimer(44, 317650)
local timerDarkOverload      = mod:NewBuffActiveTimer(8, 317650)
local timerShadowsCD         = mod:NewCDTimer(39, 317674, nil, mod:IsRanged() and not mod:IsPhysical())
local timerBarrageCD         = mod:NewCDTimer(85, 317685)
local timerStaggeringBlowCD  = mod:NewCDTimer(35, 317673, nil, mod:IsMelee())
local timerMagicFel          = mod:NewBuffActiveTimer(15, 317681, nil, mod:IsTank())
local timerMagicDark         = mod:NewBuffActiveTimer(15, 317683, nil, mod:IsTank())
local timerMagicCD           = mod:NewCDTimer(53, 317675, nil, mod:IsTank())

local felTargets = {}
local darkTargets = {}
local felIcon = 8
local darkIcon = 5

mod:AddBoolOption("RangeFrame", true)
mod:AddSetIconOption("SetIconOnFelTargets", 317666, {8,7,6}, true)
mod:AddSetIconOption("SetIconOnDarkTargets", 317662, {5,4,3}, true)

function mod:OnCombatStart(delay)
    timerFelCD:Start(20-delay)
    timerDarkCD:Start(42-delay)
    timerShadowsCD:Start(-delay)
    warnBarrageSoon:Schedule(55)
    timerBarrageCD:Start(60-delay)
    timerStaggeringBlowCD:Start(-delay)
    timerMagicCD:Start(50-delay)
    table.wipe(felTargets)
    table.wipe(darkTargets)
    felIcon = 8
    darkIcon = 5
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(317673) then
        warnStaggeringBlow:Show()
        timerStaggeringBlowCD:Start()
    elseif args:IsSpellID(317674) then
        specWarnShadows:Show()
        timerShadowsCD:Start()
    elseif args:IsSpellID(317685) then
        specWarnBarrage:Show()
        warnBarrageSoon:Schedule(80)
        timerBarrageCD:Start()
    end
end

function mod:WarnFel()
    warnFelOverload:Show(table.concat(felTargets, "<, >"))
    table.wipe(felTargets)
    felIcon = 8
end

function mod:WarnDark()
    warnDarkOverload:Show(table.concat(darkTargets, "<, >"))
    table.wipe(darkTargets)
    darkIcon = 5
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(317650) and self:AntiSpam(3) then
        warnDarkActivation:Show()
        timerFelCD:Start()
        if self.Options.RangeFrame then
            DBM.RangeCheck:Show(8)
        end
    elseif args:IsSpellID(317653) and self:AntiSpam(3) then
        warnFelActivation:Show()
        timerDarkCD:Start()
        if self.Options.RangeFrame then
            DBM.RangeCheck:Show(8)
        end
    elseif args:IsSpellID(317666) then
        if args:IsPlayer() then
            self:PlaySound("t_zaminirovali", "t_zakazali", "t_4pokusheniya", "t_ubit")
            specWarnFelOverload:Show()
            yellFel:Yell()
        end
        felTargets[#felTargets + 1] = args.destName
        if self.Options.SetIconOnFelTargets then
            self:SetIcon(args.destName, felIcon)
        end
        timerFelOverload:Start(args.destName)
        felIcon = felIcon - 1
        self:UnscheduleMethod("WarnFel")
        self:ScheduleMethod(0.1, "WarnFel")
    elseif args:IsSpellID(317662) then
        if args:IsPlayer() then
            self:PlaySound("t_zaminirovali", "t_zakazali", "t_4pokusheniya", "t_ubit")
            specWarnDarkOverload:Show()
            yellDark:Yell()
        end
        darkTargets[#darkTargets + 1] = args.destName
        if self.Options.SetIconOnDarkTargets then
            self:SetIcon(args.destName, darkIcon)
        end
        timerDarkOverload:Start(args.destName)
        darkIcon = darkIcon - 1
        self:UnscheduleMethod("WarnDark")
        self:ScheduleMethod(0.1, "WarnDark")
    elseif args:IsSpellID(317645) and args:IsPlayer() then
        if self:AntiSpam(5, "fel") then
            self:PlaySound("s_tormozi")
        end
        specWarnSpillingFel:Show()
    elseif args:IsSpellID(317641) and args:IsPlayer() then
        if self:AntiSpam(5, "dark") then
            self:PlaySound("s_yehay_bochka")
        end
        specWarnEnvelopingDark:Show()
    elseif args:IsSpellID(317681) then
        warnMagicFel:Show(args.destName)
        timerMagicFel:Start(args.destName)
        timerMagicCD:Start()
    elseif args:IsSpellID(317683) then
        warnMagicDark:Show(args.destName)
        timerMagicDark:Start(args.destName)
        timerMagicCD:Start()
    end
end

function mod:SPELL_AURA_REMOVED(args)
    if args:IsSpellID(317650, 317653) and args:IsPlayer() then
        if self.Options.RangeFrame then
            DBM.RangeCheck:Hide()
        end
    elseif args:IsSpellID(317662) then
        if self.Options.SetIconOnDarkTargets then
            self:RemoveIcon(args.destName)
        end
    elseif args:IsSpellID(317666) then
        if self.Options.SetIconOnFelTargets then
            self:RemoveIcon(args.destName)
        end
    end
end

function mod:SPELL_DAMAGE(args)
    if args:IsSpellID(317670, 317672) and args:IsPlayer() and self:AntiSpam(5, "gibdd") then
        self:PlaySound("gibdd")
    end
end

function mod:OnCombatEnd(wipe)
    if self.Options.RangeFrame then
        DBM.RangeCheck:Hide()
    end
    table.wipe(felTargets)
    table.wipe(darkTargets)
    felIcon = 8
    darkIcon = 5
end