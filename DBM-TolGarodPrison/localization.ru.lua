﻿if GetLocale() ~= "ruRU" then return end

local L

-- Gogonash
L = DBM:GetModLocalization("Gogonash")

L:SetGeneralLocalization{
    name = "Гогонаш"
}

L:SetTimerLocalization {
}

L:SetWarningLocalization{
    WarnSearedFlesh      = "%s на |3-5(>%s<) (%s)"
}

L:SetOptionLocalization{
    WarnSearedFlesh      = DBM_CORE_AUTO_ANNOUNCE_OPTIONS.spell:format(317542, GetSpellInfo(317542) or "unknown"),
    AchievementCheckFear = "Объявлять о провале $achievement:6895\n(требуются права помощника)",
}

L:SetMiscLocalization{
    FearFail             = "%s провалено (%s)"
}

-- Mind Flayer Ktrax
L = DBM:GetModLocalization("Ktrax")

L:SetGeneralLocalization{
    name = "Поглотитель разума Ктракс"
}

L:SetTimerLocalization {
}

L:SetWarningLocalization{
}

L:SetOptionLocalization{
    AchievementCheckAdds    = "Объявлять об убийстве 40 мобов для  $achievement:6898\n(требуются права помощника)"
}
-- 
L:SetMiscLocalization{
    AddsDone                = "%s сделано"
}

-- Magic Devourer
L = DBM:GetModLocalization("MagicDevourer")

L:SetGeneralLocalization{
    name = "Пожиратель Магии"
}

L:SetTimerLocalization {
}

L:SetWarningLocalization{
}

L:SetOptionLocalization{
    RangeFrame          = "Отображать окно проверки дистанции когда на Вас\n $spell:317650 или $spell:317653"
}

L:SetMiscLocalization{
}

