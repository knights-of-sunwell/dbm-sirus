local mod = DBM:NewMod("Alar", "DBM-TheEye")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 163 $"):sub(12, -3))

mod:SetCreatureID(19514)
mod:RegisterCombat("combat",19514)

mod:RegisterEvents(
    "SPELL_CAST_SUCCESS",
    "SPELL_CAST_START",
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_APPLIED_DOSE",
    "UNIT_TARGET"
)
--normal
local warnPlatSoon         = mod:NewAnnounce("WarnPlatSoon", 3, 46599)
local warnFeatherSoon      = mod:NewSoonAnnounce(34229, 4)
local warnBombSoon         = mod:NewSoonAnnounce(35181, 3)
local warnBomb             = mod:NewTargetAnnounce(35181, 3)

local specWarnFeather      = mod:NewSpecialWarningSpell(34229, not mod:IsRanged())
local specWarnBomb         = mod:NewSpecialWarningYou(35181)
local specWarnPatch        = mod:NewSpecialWarningMove(35383)

local timerNextPlat        = mod:NewTimer(36, "TimerNextPlat", 46599)
local timerFeather         = mod:NewCastTimer(10, 34229)
local timerNextFeather     = mod:NewCDTimer(180, 34229)
local timerNextCharge      = mod:NewCDTimer(22, 35412)
local timerNextBomb        = mod:NewCDTimer(46, 35181)

--heroic
local warnFlameBuffet                = mod:NewAnnounce("WarnFlameBuffet", 2, 308628, mod:IsTank())
local warnPhase2                    = mod:NewPhaseAnnounce(2, 2)

local specWarnFlameBuffet           = mod:NewSpecialWarning("SpecWarnFlameBuffet", mod:IsTank() or IsRaidLeader())
local specWarnFlameFall             = mod:NewSpecialWarningRun(308987 , not mod:IsTank())
local specWarnFlameFallm            = mod:NewSpecialWarningMove(308987 , mod:IsMelee())

local timerFlameFall                = mod:NewCastTimer(5, 308987)
local timerNextFlameFall            = mod:NewCDTimer(35, 308987)
local timerRagingFlame              = mod:NewCastTimer(20, 308640)
local timerPhoenixMark              = mod:NewTimer(20, "TimerPhoenixMark", 308663)
local timerPhoenixYell              = mod:NewCDTimer(13, 308671)

local berserkTimer          = mod:NewBerserkTimer(1200)

local setIcons = false
local phase = 1
local phoenixGUIDs = {}

function mod:Platform()
    timerNextPlat:Start()
    warnPlatSoon:Schedule(33)
    self:UnscheduleMethod("Platform")
    self:ScheduleMethod(36, "Platform")
end

function mod:PhoenixIcon()
    if DBM:GetRaidRank() >= 1 then
        for i = 1, GetNumRaidMembers() do
            if phoenixGUIDs[UnitGUID("raid"..i.."target")] then
                SetRaidTarget("raid"..i.."target", 8 + 308663 - phoenixGUIDs[UnitGUID("raid"..i.."target")])
            end
        end
    end
end

function mod:stopSearch()
    setIcons = false
    table.wipe(phoenixGUIDs)
end

function mod:OnCombatStart(delay)
    if mod:IsDifficulty("normal25") then
      berserkTimer:Start()
      timerNextPlat:Start(39)
      timerNextFeather:Start()
      warnPlatSoon:Schedule(36)
      warnFeatherSoon:Schedule(169)
      self:ScheduleMethod(39, "Platform")
    elseif mod:IsDifficulty("heroic25") then
        timerNextFlameFall:Start(20)
        setIcons = false
        phase = 1
        table.wipe(phoenixGUIDs)
    end
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(34229) then
        timerFeather:Start()
        timerNextFeather:Start()
        timerNextPlat:Cancel()
        timerNextPlat:Schedule(10)
        self:UnscheduleMethod("Platform")
        self:ScheduleMethod(46, "Platform")
    elseif args:IsSpellID(35181) then
        warnBomb:Show(args.destName)
        timerNextBomb:Start()
        if args:IsPlayer() then
            specWarnBomb:Show()
        end
    elseif args:IsSpellID(308640) then
        setIcons = true
        phase = 2
        table.wipe(phoenixGUIDs)
        timerNextFlameFall:Start(58)
        timerRagingFlame:Start()
        warnPhase2:Show()
    end
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(34342) then
        timerFeather:Cancel()
        timerNextFeather:Cancel()
        timerNextPlat:Cancel()
        self:UnscheduleMethod("Platform")
        warnPlatSoon:Cancel()
        warnFeatherSoon:Cancel()
        timerNextCharge:Start()
        timerNextBomb:Start()
        warnBombSoon:Schedule(43)
    elseif args:IsSpellID(308987) then
        timerFlameFall:Start()
        specWarnFlameFall:Schedule(4)
        specWarnFlameFallm:Show()
        timerNextFlameFall:Cancel()
        timerNextFlameFall:Schedule(5)
    elseif args:IsSpellID(308663,308664,308665,308666) then
        phoenixGUIDs[args.sourceGUID] = args.spellId
        timerPhoenixMark:Start()
        timerPhoenixYell:Start(45)
        self:ScheduleMethod(7, "stopSearch")
    elseif args:IsSpellID(308671) then
        timerPhoenixYell:Start()
    end
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(35383) and args:IsPlayer() then
        specWarnPatch:Show()
    elseif args:IsSpellID(308628) then
        warnFlameBuffet:Show(args.spellName, args.destName, args.amount or 1)
        if args.amount and args.amount >=10 then
            specWarnFlameBuffet:Show(args.amount, args.destName)
        end
    end
end

mod.SPELL_AURA_APPLIED_DOSE = mod.SPELL_AURA_APPLIED

function mod:UNIT_TARGET()
    if setIcons then
        self:PhoenixIcon()
    end
end
