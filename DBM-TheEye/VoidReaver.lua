local mod = DBM:NewMod("VoidReaver", "DBM-TheEye")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 163 $"):sub(12, -3))

mod:SetCreatureID(19516)
mod:RegisterCombat("yell", L.YellPull)
mod:SetUsedIcons(8,7,6)

mod:RegisterEvents(
    "SPELL_CAST_SUCCESS",
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_APPLIED_DOSE",
    "SPELL_AURA_REMOVED",
    "SPELL_SUMMON",
    "SPELL_DAMAGE"
)
--normal
local timerNextPounding     = mod:NewCDTimer(14, 34162)
local timerNextKnockback    = mod:NewCDTimer(30, 25778)

--heroic
local timerMatrix           = mod:NewTimer(60, "TimerMatrix", 308465)
local timerReload           = mod:NewTimer(60, "TimerReload", 308473)
local timerArcaneOrbCD      = mod:NewCDTimer(25, 308466)
local timerArcaneSens       = mod:NewBuffActiveTimer(10, 308469)

local warnMatrixEndSoon     = mod:NewAnnounce("WarnMatrixEndSoon", 3)
local warnReloadEndSoon     = mod:NewAnnounce("WarnReloadEndSoon", 3)
local warnPhase2            = mod:NewAnnounce("WarnPhase2", 1)
local specWarnReloadEnd     = mod:NewSpecialWarning("SpecWarnReloadEnd", mod:IsTank())
local warnMagnetic          = mod:NewTargetAnnounce(308467, 3)
local specWarnMagnetic      = mod:NewSpecialWarningYou(308467)

local berserkTimer          = mod:NewBerserkTimer(600)

local orbTrigger = true
local phase = 1
local magneticTargets = {}

mod:AddSetIconOption("SetIconOnMagnetic", 308467, {8, 7, 6}, true)

function mod:OnCombatStart(delay)
    phase = 1
    self:PlaySound("basscannon")
    table.wipe(magneticTargets)
    if mod:IsDifficulty("normal25") then
      berserkTimer:Start()
      timerNextPounding:Start()
      timerNextKnockback:Start()
    end
end


function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(25778) then
        timerNextKnockback:Start()
    elseif args:IsSpellID(34162) then
        timerNextPounding:Start()
    end
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(308465) then
        timerMatrix:Start()
        warnMatrixEndSoon:Schedule(55)
        timerArcaneOrbCD:Start()
        orbTrigger = true
    elseif args:IsSpellID(308473) then
        timerReload:Start()
        warnReloadEndSoon:Schedule(55)
        specWarnReloadEnd:Schedule(57)
    elseif args:IsSpellID(308469) then
        timerArcaneSens:Start()
    elseif args:IsSpellID(308479) and phase == 1 then
        phase = 2 
        warnPhase2:Show()
        timerMatrix:Cancel()
        warnMatrixEndSoon:Cancel()
    elseif args:IsSpellID(308467) then
      if args:IsPlayer() then
              self:PlaySound("welcome")      -- Welcome to the club buddy... *fucking slap*
            specWarnMagnetic:Show()
        end
        magneticTargets[#magneticTargets + 1] = args.destName
        if #magneticTargets >= 3 then
            warnMagnetic:Show(table.concat(magneticTargets, "<, >"))
            if self.Options.SetIconOnMagnetic then
                table.sort(magneticTargets, function(v1,v2) return DBM:GetRaidSubgroup(v1) < DBM:GetRaidSubgroup(v2) end)
                local icons = 8
                for i, v in ipairs(magneticTargets) do
                    self:SetIcon(v, icons)
                    icons = icons - 1
                end
            end
            table.wipe(magneticTargets)
        end
    end
end

function mod:SPELL_AURA_APPLIED_DOSE(args)
    if args:IsSpellID(308469) then
        timerArcaneSens:Start()
    end
end

function mod:SPELL_DAMAGE(args)
    if args:IsSpellID(308470) and args.overkill > 0 and DBM:GetRaidUnitId(args.destName) then
        self:PlaySound("dmg", "fiasco", "resist", "mp_scratch", "baklazhan")
    end
end

function mod:SPELL_AURA_REMOVED(args)
    if args:IsSpellID(308465) then
      self:PlaySound("run", "tobecon", "ya_vas_ne_zval", "moredots", "fear2")
    elseif args:IsSpellID(308473) then
      self:PlaySound("optics_online", "run", "fear2")
    elseif args:IsSpellID(308467) then
      self:SetIcon(args.destName, 0)
    end
end

function mod:SPELL_SUMMON(args)
    if args:IsSpellID(308466) then
      if (phase == 1 and orbTrigger) then 
        timerArcaneOrbCD:Start()
        orbTrigger = false
      elseif phase == 2 then
        timerArcaneOrbCD:Start(33)
      end
    end
end
