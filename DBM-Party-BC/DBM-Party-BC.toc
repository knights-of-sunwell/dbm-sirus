﻿## Interface: 30300
## Title:|cffffe00a<|r|cffff7d0aDBM|r|cffffe00a>|r |cff69ccf0Party-BC|r
## Author: Ritual
## LoadOnDemand: 1
## RequiredDeps: DBM-Core
## SavedVariablesPerCharacter: DBMPartyBC_SavedVars, DBMPartyBC_SavedStats
## X-DBM-Mod: 1
## X-DBM-Mod-Category: BC
## X-DBM-Mod-SubCategories: Auchenai Crypts, Hellfire Ramparts, Mana-Tombs, The Blood Furnace, Underbog, The Steamvault, The Shattered Halls, Sethekk Halls, The Slave Pens, The Arcatraz, Shadow Labyrinth, The Botanica, The Mechanar, Old Hillsbrad Foothills, Magisters' Terrace, Black Morass
## X-DBM-Mod-SubCategories-ruRU: Аукенайские гробницы, Бастионы Адского Пламени, Гробницы Маны, Кузня Крови, Нижетопь, Паровое подземелье, Разрушенные залы, Сетеккские залы, Узилище, Аркатрац, Темный лабиринт, Ботаника, Механар, Старые предгорья Хилсбрада, Терраса Магистров, Черные топи
## X-DBM-Mod-Name: PartyBC
## X-DBM-Mod-Name-ruRU: РБК
## X-DBM-Mod-Sort: 307
## X-DBM-Mod-LoadZone: Auchenai Crypts, Hellfire Ramparts, Mana-Tombs, The Blood Furnace, Underbog, The Steamvault, The Shattered Halls, Sethekk Halls, The Slave Pens, The Arcatraz, Shadow Labyrinth, The Botanica, The Mechanar, Old Hillsbrad Foothills, Magisters' Terrace, Black Morass
## X-DBM-Mod-LoadZone-ruRU: Аукенайские гробницы, Бастионы Адского Пламени, Гробницы Маны, Кузня Крови, Нижетопь, Паровое подземелье, Разрушенные залы, Сетеккские залы, Узилище, Аркатрац, Темный лабиринт, Ботаника, Механар, Старые предгорья Хилсбрада, Терраса Магистров, Черные топи

localization.ru.lua

Auct_Crypts\Anzu.lua
Auct_Crypts\ACTrash.lua
Auct_Crypts\Shirrak.lua
Auct_Crypts\Exarh.lua

Auct_ManaTombs\MTTrash.lua
Auct_ManaTombs\Pandemonius.lua
Auct_ManaTombs\Tavarok.lua
Auct_ManaTombs\Yor.lua
Auct_ManaTombs\Shaffar.lua

Auct_SethekkHalls\SHTrash.lua
Auct_SethekkHalls\Syth.lua
Auct_SethekkHalls\Anzu.lua
Auct_SethekkHalls\Ikiss.lua

Auct_ShadowLabyrinth\Hellmaw.lua
Auct_ShadowLabyrinth\Inciter.lua
Auct_ShadowLabyrinth\Vorpil.lua
Auct_ShadowLabyrinth\Murmur.lua


Coil_Slavepens\SPTrash.lua
Coil_Slavepens\Mennu.lua
Coil_Slavepens\Rokmar.lua
Coil_Slavepens\Quagmirran.lua

Coil_Steamvault\Thespia.lua
Coil_Steamvault\Steamrigger.lua
Coil_Steamvault\Kalithresh.lua

Coil_Underbog\UTrash.lua
Coil_Underbog\Hungarfen.lua
Coil_Underbog\Ghazan.lua
Coil_Underbog\Muselek.lua
Coil_Underbog\BlackStalker.lua


CoT_BlackMorass\PortalTimers.lua
CoT_BlackMorass\Deja.lua
CoT_BlackMorass\Temporus.lua
CoT_BlackMorass\Aeonus.lua

CoT_OldHillsbrad\Drake.lua
CoT_OldHillsbrad\Skarloc.lua
CoT_OldHillsbrad\EpochHunter.lua


Hellfire_BloodFurnace\BFTrash.lua
Hellfire_BloodFurnace\TheMaker.lua
Hellfire_BloodFurnace\Broggok.lua
Hellfire_BloodFurnace\Kelidan.lua

Hellfire_Ramp\HRTrash.lua
Hellfire_Ramp\Watchkeeper.lua
Hellfire_Ramp\Omor.lua
Hellfire_Ramp\Vazruden.lua

Hellfire_ShatteredHalls\Porung.lua
Hellfire_ShatteredHalls\Nethekurse.lua
Hellfire_ShatteredHalls\Omrogg.lua
Hellfire_ShatteredHalls\Kargath.lua


MagistersTerrace\Selin.lua
MagistersTerrace\Vexallus.lua
MagistersTerrace\Delrissa.lua
MagistersTerrace\Kaelthas.lua


TK_Arcatraz\ArTrash.lua
TK_Arcatraz\Zereketh.lua
TK_Arcatraz\Dalliah.lua
TK_Arcatraz\Soccothrates.lua
TK_Arcatraz\Skyriss.lua

TK_Botanica\Sarannis.lua
TK_Botanica\Freywinn.lua
TK_Botanica\Thorngrin.lua
TK_Botanica\Laj.lua
TK_Botanica\WarpSplinter.lua

TK_Mechanar\Gyrokill.lua
TK_Mechanar\Ironhand.lua
TK_Mechanar\Capacitus.lua
TK_Mechanar\Sepethrea.lua
TK_Mechanar\Pathaleon.lua