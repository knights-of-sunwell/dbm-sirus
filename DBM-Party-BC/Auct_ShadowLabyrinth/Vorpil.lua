local mod = DBM:NewMod("Vorpil", "DBM-Party-BC", 11)
local L = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 128 $"):sub(12, -3))

mod:SetCreatureID(18732)
mod:RegisterCombat("combat")

mod:RegisterEvents(
    "SPELL_AURA_APPLIED"
)


local timerTeleportCD    = mod:NewCDTimer(30, 33563)
local timerBanish         = mod:NewTargetTimer(8, 38791)
local timerBanishCD        = mod:NewCDTimer(16, 38791)

local warnTeleport        = mod:NewSpellAnnounce(33563)
local warnBanish        = mod:NewTargetAnnounce(38791)

local specWarnRain        = mod:NewSpecialWarningInterupt(39363)

function mod:RainWarn()
    self:PlaySound("rain")               -- (It's Raining Man - Мужицкий дождь)
    warnTeleport:Show()
    specWarnRain:Show()
    timerTeleportCD:Start()
end

function mod:OnCombatStart(delay)
    timerTeleportCD:Start(45-delay)
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(33563) then
        self:UnscheduleMethod("RainWarn")
        self:ScheduleMethod(0.1, "RainWarn")
    elseif args:IsSpellID(38791) then
        warnBanish:Show(args.destName)
        timerBanish:Start(args.destName)
        timerBanishCD:Start()
    end
end