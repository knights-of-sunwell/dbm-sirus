local mod = DBM:NewMod("WarpSplinter", "DBM-Party-BC", 12)
local L = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 128 $"):sub(12, -3))

mod:SetCreatureID(17977)
mod:RegisterCombat("combat")

mod:RegisterEvents(
    "SPELL_CAST_SUCCESS",
    "SPELL_CAST_START"
)

local warnTreants    = mod:NewSpellAnnounce(34727)
local warnStomp      = mod:NewSpellAnnounce(34716)

local timerTreants   = mod:NewNextTimer(45, 34727)
local timerStomp     = mod:NewBuffActiveTimer(5, 34716)
local timerStompCD   = mod:NewCDTimer(32, 34716)
local timerVolleyCD  = mod:NewCDTimer(32, 39133)

function mod:TimerTreantsStart()
    timerTreants:Start()
    warnTreants:Show()
    self:UnscheduleMethod("TimerTreantsStart")
    self:ScheduleMethod(45, "TimerTreantsStart")
end

function mod:OnCombatStart(delay)
    timerTreants:Start()
    self:ScheduleMethod(45, "TimerTreantsStart")
    timerVolleyCD:Start(10)
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(34716) then
        warnStomp:Show()
        timerStomp:Start()
        timerStompCD:Start()
    end
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(39133) then
        timerVolleyCD:Start()
    end
end