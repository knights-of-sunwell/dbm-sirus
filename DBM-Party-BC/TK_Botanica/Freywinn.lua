local mod = DBM:NewMod("Freywinn", "DBM-Party-BC", 12)
local L = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 147 $"):sub(12, -3))

mod:SetCreatureID(17975)
mod:RegisterCombat("combat")

mod:RegisterEvents(
    "SPELL_CAST_SUCCESS",
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_REMOVED",
    "SPELL_CAST_START"
)

local warnTreeForm   =     mod:NewSpellAnnounce(34551)

local timerTreeFormCD =     mod:NewCDTimer(30, 34551)
local timerSeedlingCD =     mod:NewTimer(30, "TimerSeedling", 25299)
local timerTreeForm   =      mod:NewBuffActiveTimer(45, 34551)

function mod:OnCombatStart()
    timerTreeFormCD:Start()
    timerSeedlingCD:Start()
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(34551) then 
        warnTreeForm:Show()
        timerTreeForm:Show()
    end
end

function mod:SPELL_AURA_REMOVED(args)
    if args:IsSpellID(34551) then 
        timerTreeForm:Stop()
        timerTreeFormCD:Start()
        timerSeedlingCD:Start()
    end
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(34761, 34762, 34763) then 
        timerSeedlingCD:Start()
    end
end