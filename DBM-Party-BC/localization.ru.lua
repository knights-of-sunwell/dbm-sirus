﻿if GetLocale() ~= "ruRU" then return end

local L

local spell                = "%s"                
local debuff            = "%s: >%s<"            
local spellCD            = "Восстановление %s"
local spellSoon            = "Скоро %s"
local optionWarning        = "Предупреждение для %s"
local optionPreWarning    = "Предупреждать заранее о %s"
local optionSpecWarning    = "Спец-предупреждение для %s"
local optionTimerCD        = "Отсчет времени до восстановления %s"
local optionTimerDur    = "Отсчет времени для %s"
local optionTimerCast    = "Отсчет времени применения заклинания %s"

----------------------------------
--  Auchenai Crypts             --
----------------------------------
--  Thrash  --
-----------------------
L = DBM:GetModLocalization("ACTrash")

L:SetGeneralLocalization({
    name = "АГ треш"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

----------------------------------
--  Shirrak  --
-----------------------
L = DBM:GetModLocalization("Shirrak")

L:SetGeneralLocalization({
    name = "Ширрак Страж Мертвых"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
    EmoteFire = " оказывается в зоне внимания |3-1(%s)."
})

----------------------------------
--  Exarh  --
-----------------------
L = DBM:GetModLocalization("Exarh")

L:SetGeneralLocalization({
    name = "Экзарх Маладаар"
})

L:SetWarningLocalization({
    WarnSoul = "Украденная душа на %s"
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
    WarnSoul = DBM_CORE_AUTO_ANNOUNCE_OPTIONS.target:format(32346, GetSpellInfo(32346))
})

L:SetMiscLocalization({
})

----------------------------------
--  Hellfire Ramparts           --
----------------------------------
--  Thrash  --
-----------------------
L = DBM:GetModLocalization("HRTrash")

L:SetGeneralLocalization({
    name = "Бастионы треш"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
    Dogs = "Призыв бойцовских псов"
})

L:SetOptionLocalization({
    Dogs = "Отсчет времени до призыва бойцовских псов"
})

L:SetMiscLocalization({
    yellDogs = "Незваные гости! Задержите их, а я спущу бойцовых псов!"
})

----------------------------------
--  Watchkeeper  --
-----------------------
L = DBM:GetModLocalization("Watchkeeper")

L:SetGeneralLocalization({
    name = "Начальник стражи Гарголмар"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
    TimerWound = "Смертельная рана %s"
})

L:SetOptionLocalization({
    TimerWound = DBM_CORE_AUTO_TIMER_OPTIONS.target:format(36814, GetSpellInfo(36814))
})

L:SetMiscLocalization({
})

----------------------------------
--  Omor  --
-----------------------
L = DBM:GetModLocalization("Omor")

L:SetGeneralLocalization({
    name = "Омор Неодолимый"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
})

----------------------------------
--  Vazruden  --
-----------------------
L = DBM:GetModLocalization("Vazruden")

L:SetGeneralLocalization({
    name = "Вазруден Глашатай"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
    EmoteNazan = "Назан спускается с небес."
})

----------------------------------
--  The Slave Pens           --
----------------------------------
--  Thrash  --
-----------------------
L = DBM:GetModLocalization("SPTrash")

L:SetGeneralLocalization({
    name = "Узилище треш"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
})

----------------------------------
--  Mennu  --
-----------------------
L = DBM:GetModLocalization("Mennu")

L:SetGeneralLocalization({
    name = "Менну Предатель"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
})

----------------------------------
--  Rokmar  --
-----------------------
L = DBM:GetModLocalization("Rokmar")

L:SetGeneralLocalization({
    name = "Рокмар Трескун"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
})

----------------------------------
--  Quagmirran  --
-----------------------
L = DBM:GetModLocalization("Quagmirran")

L:SetGeneralLocalization({
    name = "Зыбун"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
})

----------------------------------
--  TheBloodFurnace           --
----------------------------------
--  Thrash  --
-----------------------
L = DBM:GetModLocalization("BFTrash")

L:SetGeneralLocalization({
    name = "Кузня крови треш"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
})

----------------------------------
--  TheMaker  --
-----------------------
L = DBM:GetModLocalization("TheMaker")

L:SetGeneralLocalization({
    name = "Мастер"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
    yellPull = "Не мешайте мне работать!"
})

----------------------------------
--  Broggok  --
-----------------------
L = DBM:GetModLocalization("Broggok")

L:SetGeneralLocalization({
    name = "Броггок"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
    yellPull = "Заходите, незваные гости..."
})

----------------------------------
--  Keli'dan  --
-----------------------
L = DBM:GetModLocalization("Kelidan")

L:SetGeneralLocalization({
    name = "Кели'дан Разрушитель"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
    Explosion = "Взрыв!"
})

L:SetOptionLocalization({
    Explosion = DBM_CORE_AUTO_TIMER_OPTIONS.cast:format(37371, GetSpellInfo(37371))
})

L:SetMiscLocalization({
})

----------------------------------
--  Mana Tombs           --
----------------------------------
--  Thrash  --
-----------------------
L = DBM:GetModLocalization("MTTrash")

L:SetGeneralLocalization({
    name = "Гробницы маны треш"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
})

----------------------------------
--  Pandemonius  --
-----------------------
L = DBM:GetModLocalization("Pandemonius")

L:SetGeneralLocalization({
    name = "Пандемоний"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
})

----------------------------------
--  Tavarok  --
-----------------------
L = DBM:GetModLocalization("Tavarok")

L:SetGeneralLocalization({
    name = "Таварок"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
})

----------------------------------
--  Yor  --
-----------------------
L = DBM:GetModLocalization("Yor")

L:SetGeneralLocalization({
    name = "Йор"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
})

----------------------------------
--  Nexus-Prince Shaffar  --
-----------------------
L = DBM:GetModLocalization("Shaffar")

L:SetGeneralLocalization({
    name = "Принц Шаффар"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
    TimerEtherealOrb = "Следующий маяк",
    TimerEtherealSpawn = "Вызов ученика"
})

L:SetOptionLocalization({
    TimerEtherealOrb = "Отсчет времени до вызова следующего эфирального маяка",
    TimerEtherealSpawn = "Отсчет времени до вызова эфирального ученика"
})

L:SetMiscLocalization({
})

----------------------------------
--  Sethekk Halls           --
----------------------------------
--  Thrash  --
-----------------------
L = DBM:GetModLocalization("SHTrash")

L:SetGeneralLocalization({
    name = "Сетеккские залы треш"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
})

----------------------------------
--  Syth  --
-----------------------
L = DBM:GetModLocalization("Syth")

L:SetGeneralLocalization({
    name = "Темнопряд Сит"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
    yellSummon = "У меня свои звер-ррр-рюшки есть!"
})

------------
--  Anzu  --
------------
L = DBM:GetModLocalization("Anzu")

L:SetGeneralLocalization({
    name = "Анзу"
})

L:SetWarningLocalization({
    warnBirds    = "Brood of Anzu soon",
    warnStoned    = "%s returned to stone"
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
    warnBirds    = "Show pre-warning for Brood of Anzu",
    warnStoned    = "Show warning for spirits returning to stone"
})

L:SetMiscLocalization({
    BirdStone    = "%s обращается в камень."
})

----------------------------------
--  Ikiss  --
-----------------------
L = DBM:GetModLocalization("Ikiss")

L:SetGeneralLocalization({
    name = "Король воронов Айкисс"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
    Explosion = "Взрыв!"
})

L:SetOptionLocalization({
    Explosion = "Отсчет времени до взрыва"
})

L:SetMiscLocalization({
})

----------------------------------
--  Underbog           --
----------------------------------
--  Thrash  --
-----------------------
L = DBM:GetModLocalization("UTrash")

L:SetGeneralLocalization({
    name = "Нижетопь треш"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
})

----------------------------------
--  Hungarfen  --
-----------------------
L = DBM:GetModLocalization("Hungarfen")

L:SetGeneralLocalization({
    name = "Топеглад"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
})

----------------------------------
--  Ghazan  --
-----------------------
L = DBM:GetModLocalization("Ghazan")

L:SetGeneralLocalization({
    name = "Газ'ан"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
})

----------------------------------
--  Muselek  --
-----------------------
L = DBM:GetModLocalization("Muselek")

L:SetGeneralLocalization({
    name = "Владыка болот Мусел'ек"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
})

----------------------------------
--  BlackStalker  --
-----------------------
L = DBM:GetModLocalization("BlackStalker")

L:SetGeneralLocalization({
    name = "Черная Охотница"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
})

----------------------------------
--  Arcatraz          --
----------------------------------
--  Thrash  --
-----------------------
L = DBM:GetModLocalization("ArTrash")

L:SetGeneralLocalization({
    name = "Аркатрац треш"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
})

----------------------------------
--  Zereketh  --
-----------------------
L = DBM:GetModLocalization("Zereketh")

L:SetGeneralLocalization({
    name = "Зерекет Бездонный"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
})

----------------------------------
--  Dalliah  --
-----------------------
L = DBM:GetModLocalization("Dalliah")

L:SetGeneralLocalization({
    name = "Даллия Глашатай Судьбы"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
})

----------------------------------
--  Soccothrates  --
-----------------------
L = DBM:GetModLocalization("Soccothrates")

L:SetGeneralLocalization({
    name = "Провидец Гнева Соккорат"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
})

----------------------------------
--  Skyriss  --
-----------------------
L = DBM:GetModLocalization("Skyriss")

L:SetGeneralLocalization({
    name = "Предвестник Скайрисс"
})

L:SetWarningLocalization({
    WarnSplitSoon = "Скоро раздвоение",
    WarnSplit     = "Раздвоение"
})

L:SetTimerLocalization({
    TimerNxtPrisoner = "Следующий пленник",
    TimerPullBoss = "Активация босса"
})

L:SetOptionLocalization({
    TimerNxtPrisoner = "Отображать время до появления следующего пленника.",
    TimerPullBoss = "Отображать таймер до активации босса.",
    WarnSplitSoon = DBM_CORE_AUTO_ANNOUNCE_OPTIONS.soon:format(39019, GetSpellInfo(39019)),
    WarnSplit     = DBM_CORE_AUTO_ANNOUNCE_OPTIONS.spell:format(39019, GetSpellInfo(39019))
})

L:SetMiscLocalization({
    Pull = "Я знаю, принцу это не понравится, но... Я был не в себе. Мне пришлось их выпустить! Великий говорит со мной, понимаете? Постойте... чужаки! Кель'Тас вас не посылал! Ладно... Скажу принцу, что это вы выпустили пленников.",
    Prisoner2 = "Смотрите, вот и еще одно ужасное создание небывалой мощи!",
    Prisoner3 = "Как я здесь очутился, Негатрон меня раздери! И кто это свалился – йааагггх! – на мою больную голову?!",
    Prisoner4 = "Осталась последняя камера. Да, о великий, сию минуту!",
    Wat     = "Да-да... следующий! Ваша воля для меня – закон!",
    Split    = "Мы бесчисленны, как звезды! Мы заполоним вселенную!"
})

----------------------------------
--  Shadow Labyrinth  --
-----------------------
----------------------------------
--  Ambassador Hellmaw  --
-----------------------
L = DBM:GetModLocalization("Hellmaw")

L:SetGeneralLocalization({
    name = "Посол Гиблочрев"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
})

----------------------------------
--  Blackheart the Inciter  --
-----------------------
L = DBM:GetModLocalization("Inciter")

L:SetGeneralLocalization({
    name = "Черносерд Проповедник"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
    RemoveWeaponOnMindControl   = "Убирать оружие на МК."
})

L:SetMiscLocalization({
})


--------------------------
--  Grandmaster Vorpil  --
--------------------------
L = DBM:GetModLocalization("Vorpil")

L:SetGeneralLocalization({
    name = "Великий мастер Ворпил"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

--------------
--  Murmur  --
--------------
L = DBM:GetModLocalization("Murmur")

L:SetGeneralLocalization({
    name = "Бормотун"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
    SetIconOnTouchTarget    = DBM_CORE_AUTO_ICONS_OPTION_TEXT:format(33711)
})


---------------------------
--  The Shattered Halls  --
--------------------------------
--  Grand Warlock Nethekurse  --
--------------------------------
L = DBM:GetModLocalization("Nethekurse")

L:SetGeneralLocalization({
    name = "Grand Warlock Nethekurse"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

--------------------------
--  Blood Guard Porung  --
--------------------------
L = DBM:GetModLocalization("Porung")

L:SetGeneralLocalization({
    name = "Blood Guard Porung"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

--------------------------
--  Warbringer O'mrogg  --
--------------------------
L = DBM:GetModLocalization("O'mrogg")

L:SetGeneralLocalization({
    name = "Warbringer O'mrogg"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

----------------------------------
--  Warchief Kargath Bladefist  --
----------------------------------
L = DBM:GetModLocalization("Kargath")

L:SetGeneralLocalization({
    name = "Warchief Kargath Bladefist"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

----------------------
--  The Steamvault  --
---------------------------
--  Hydromancer Thespia  --
---------------------------
L = DBM:GetModLocalization("Thespia")

L:SetGeneralLocalization({
    name = "Hydromancer Thespia"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

-----------------------------
--  Mekgineer Steamrigger  --
-----------------------------
L = DBM:GetModLocalization("Steamrigger")

L:SetGeneralLocalization({
    name = "Mekgineer Steamrigger"
})

L:SetWarningLocalization({
    WarnSummon    = "Steamrigger Mechanics incomming"
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
    WarnSummon    = "Show warning for Steamrigger Mechanics"
})

L:SetMiscLocalization({
    Mechs    = "Tune 'em up good boys!"
})

--------------------------
--  Warlord Kalithresh  --
--------------------------
L = DBM:GetModLocalization("Kalithresh")

L:SetGeneralLocalization({
    name = "Warlord Kalithresh"
})

L:SetWarningLocalization({
    WarnChannel    = "Channeling"
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
    WarnChannel    = "Show warning when channeling"
})


-------------------------------
--  Old Hillsbrad Foothills  --
-------------------------------
--  Lieutenant Drake  --
------------------------
L = DBM:GetModLocalization("Drake")

L:SetGeneralLocalization({
    name = "Lieutenant Drake"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

-----------------------
--  Captain Skarloc  --
-----------------------
L = DBM:GetModLocalization("Skarloc")

L:SetGeneralLocalization({
    name = "Captain Skarloc"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

--------------------
--  Epoch Hunter  --
--------------------
L = DBM:GetModLocalization("EpochHunter")

L:SetGeneralLocalization({
    name = "Epoch Hunter"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

------------------------
--  The Black Morass  --
------------------------
--  Chrono Lord Deja  --
------------------------
L = DBM:GetModLocalization("Deja")

L:SetGeneralLocalization({
    name = "Chrono Lord Deja"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})
----------------
--  Temporus  --
----------------
L = DBM:GetModLocalization("Temporus")

L:SetGeneralLocalization({
    name = "Temporus"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})
--------------
--  Aeonus  --
--------------
L = DBM:GetModLocalization("Aeonus")

L:SetGeneralLocalization({
    name = "Aeonus"
})

L:SetWarningLocalization({
    warnFrenzy    = "Frenzy"
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
    warnFrenzy    = "Show warning for Frenzy"
})

L:SetMiscLocalization({
    AeonusFrenzy    = "%s goes into a frenzy!"
})

---------------------
--  Portal Timers  --
---------------------
L = DBM:GetModLocalization("PT")

L:SetGeneralLocalization({
    name = "Portal Timers (CoT)"
})

L:SetWarningLocalization({
    WarnWavePortalSoon    = "New portal soon",
    WarnWavePortal        = "Portal %d",
    WarnBossPortal        = "Boss incoming"
})

L:SetTimerLocalization({
    TimerNextPortal        = "Portal %d",
})

L:SetOptionLocalization({
    WarnWavePortalSoon    = "Show pre-warning for new portal",
    WarnWavePortal        = "Show warning for new portal",
    WarnBossPortal        = "Show warning for boss incoming",
    TimerNextPortal        = "Show timer for next portal (after Boss)",
    ShowAllPortalTimers    = "Show timers for all portals (inaccurate)"
})

L:SetMiscLocalization({
    PortalCheck            = "Time Rifts Opened: (%d+)/18",
    Shielddown            = "No! Damn this feeble, mortal coil!"
})

--------------------
--  The Mechanar  --
-----------------------------
--  Gatewatcher Gyro-Kill  --
-----------------------------
L = DBM:GetModLocalization("Gyrokill")

L:SetGeneralLocalization({
    name = "Gatewatcher Gyro-Kill"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

-----------------------------
--  Gatewatcher Iron-Hand  --
-----------------------------
L = DBM:GetModLocalization("Ironhand")

L:SetGeneralLocalization({
    name = "Gatewatcher Iron-Hand"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
    JackHammer    = "%s raises his hammer menacingly..."
})

------------------------------
--  Mechano-Lord Capacitus  --
------------------------------
L = DBM:GetModLocalization("Capacitus")

L:SetGeneralLocalization({
    name = "Mechano-Lord Capacitus"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

------------------------------
--  Nethermancer Sepethrea  --
------------------------------
L = DBM:GetModLocalization("Sepethrea")

L:SetGeneralLocalization({
    name = "Nethermancer Sepethrea"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

--------------------------------
--  Pathaleon the Calculator  --
--------------------------------
L = DBM:GetModLocalization("Pathaleon")

L:SetGeneralLocalization({
    name = "Pathaleon the Calculator"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

--------------------
--  The Botanica  --
--------------------------
--  Commander Sarannis  --
--------------------------
L = DBM:GetModLocalization("Sarannis")

L:SetGeneralLocalization({
    name = "Commander Sarannis"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

------------------------------
--  High Botanist Freywinn  --
------------------------------
L = DBM:GetModLocalization("Freywinn")

L:SetGeneralLocalization({
    name = "High Botanist Freywinn"
})

L:SetWarningLocalization({
    WarnTranq    = "Tranquility - Kill Frayer Protectors"
})

L:SetTimerLocalization({
    TimerSeedling = "Саженец"
})

L:SetOptionLocalization({
    WarnTranq    = "Show warning for Tranquility",
    TimerSeedling = "Время до следующего саженца"
})

-----------------------------
--  Thorngrin the Tender  --
-----------------------------
L = DBM:GetModLocalization("Thorngrin")

L:SetGeneralLocalization({
    name = "Thorngrin the Tender"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

-----------
--  Laj  --
-----------
L = DBM:GetModLocalization("Laj")

L:SetGeneralLocalization({
    name = "Laj"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
    TimerTeleport = "Телепорт"
})

L:SetOptionLocalization({
    TimerTeleport = "Время до телепортации и призыва аддов."
})

---------------------
--  Warp Splinter  --
---------------------
L = DBM:GetModLocalization("WarpSplinter")

L:SetGeneralLocalization({
    name = "Warp Splinter"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})


--------------------------
--  Magisters' Terrace  --
--------------------------
--  Selin Fireheart  --
-----------------------
L = DBM:GetModLocalization("Selin")

L:SetGeneralLocalization({
    name = "Selin Fireheart"
})

L:SetWarningLocalization({
    warnChanneling    = "Fel Crystal channeling"
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
    warnChanneling    = "Show warning for Fel Crystal channeling"
})

L:SetMiscLocalization({
    ChannelCrystal    = "%s begins to channel from the nearby Fel Crystal..."
})

----------------
--  Vexallus  --
----------------
L = DBM:GetModLocalization("Vexallus")

L:SetGeneralLocalization({
    name = "Vexallus"
})

L:SetWarningLocalization({
    WarnEnergy    = "Pure Energy"
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
    WarnEnergy    = "Show warning for Pure Energy"
})

L:SetMiscLocalization({
    Discharge    = "Un...con...tainable."
})

--------------------------
--  Priestess Delrissa  --
--------------------------
L = DBM:GetModLocalization("Delrissa")

L:SetGeneralLocalization({
    name = "Priestess Delrissa"
})

L:SetWarningLocalization({
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
})

L:SetMiscLocalization({
    DelrissaPull    = "Annihilate them.",
    DelrissaEnd        = "Not what I had... planned."
})

------------------------------------
--  Kael'thas Sunstrider (Party)  --
------------------------------------
L = DBM:GetModLocalization("Kael")

L:SetGeneralLocalization({
    name = "Kael'thas Sunstrider (Party)"
})

L:SetWarningLocalization({
    specwarnPyroblast    = "Pyroblast - Interrupt now"
})

L:SetTimerLocalization({
})

L:SetOptionLocalization({
    specwarnPyroblast    = "Show special warning for Pyroblast (to interrupt)"
})

L:SetMiscLocalization({
    KaelP2    = "I'll turn your world... upside... down."
})

