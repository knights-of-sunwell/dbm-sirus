﻿if GetLocale() ~= "ruRU" then return end

local L

----------------
--  Norigorn  --
----------------
L = DBM:GetModLocalization("Norigorn")

L:SetGeneralLocalization{
    name = "Норигорн"
}

L:SetTimerLocalization{
}

L:SetWarningLocalization{
}

L:SetMiscLocalization{
}

L:SetOptionLocalization{
}