local mod = DBM:NewMod("Norigorn", "DBM-WorldBosses")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4181 $"):sub(12, -3))
mod:SetCreatureID(70010)

mod:RegisterCombat("combat", 70010)

mod:RegisterEvents(
    "SPELL_CAST_START",
    "SPELL_AURA_APPLIED",
    "SPELL_SUMMON"
)


local warnLightningCarnage              = mod:NewCastAnnounce(317274, 3)
local warnEarthquake                    = mod:NewCastAnnounce(317624, 4)

local specWarnEarthquake                = mod:NewSpecialWarningMove(317624)

local timerSpireCD                      = mod:NewCDTimer(60, 317267)
local timernEarthquakeCD                = mod:NewCDTimer(90, 317624)

function mod:OnCombatStart(delay)
    if self.Options.RangeFrame then
        DBM.RangeCheck:Show(10)
    end
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(317274) then
        warnLightningCarnage:Show()
    elseif args:IsSpellID(317624) then
        warnEarthquake:Show()
        timernEarthquakeCD:Start()
    end
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(317624) and args:IsPlayer()then
        specWarnEarthquake:Show()
    end
end

function mod:SPELL_SUMMON(args)
    if args:IsSpellID(317267) then
        timerSpireCD:Start()
    end
end

function mod:OnCombatEnd()
    if self.Options.RangeFrame then
        DBM.RangeCheck:Hide()
    end
end