local mod = DBM:NewMod("TerestianIllhoof", "DBM-Karazhan")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4839 $"):sub(12, -3))
mod:SetCreatureID(15688)
mod:SetUsedIcons(8)
mod:SetBossHealthInfo(
    15688, L.name
)

mod:RegisterCombat("combat")
--mod:RegisterCombat("yell", L.YellPull)
--17229--imp, for future use
--mod:RegisterKill("yell", L.YellKill)

mod:RegisterEvents(
    "SPELL_CAST_START",
    "SPELL_CAST_SUCCESS",
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_APPLIED_DOSE",
    "SPELL_AURA_REMOVED",
    "CHAT_MSG_MONSTER_YELL",
    "UNIT_DIED"
)

local warningWeakened     = mod:NewTargetAnnounce(30065, 2)
local warningImpSoon      = mod:NewSoonAnnounce(30066, 2)
local warningImp          = mod:NewSpellAnnounce(30066, 3)
local warningSacSoon      = mod:NewSoonAnnounce(30115, 3)
local warningSacrifice    = mod:NewTargetAnnounce(30115, 4)

local specWarnSacrifice   = mod:NewSpecialWarningYou(30115)

mod:AddAnnounceLine(DBM_HEROIC_MODE)
local warnSeed            = mod:NewTargetAnnounce(305360, 2)
local warnDart            = mod:NewAnnounce("WarnDart", 2, 305367, mod:IsHealer() or mod:IsTank())
local warnHand            = mod:NewTargetAnnounce(305345, 3)
local warnMark            = mod:NewTargetAnnounce(305351, 4)

local specWarnSeed        = mod:NewSpecialWarningSpell(305360, mod:IsTank())
local specWarnDart        = mod:NewSpecialWarningStack(305367, nil, 7)
local specWarnMark        = mod:NewSpecialWarningYou(305351)

local yellMark            = mod:NewYell(305351)

local timerWeakened       = mod:NewBuffActiveTimer(31, 30065)
local timerSacrifice      = mod:NewTargetTimer(30, 30115)
local timerSacrificeCD    = mod:NewNextTimer(42, 30115)
mod:AddTimerLine(DBM_HEROIC_MODE)
local timerSeedCD         = mod:NewCDTimer(7, 305360)
local timerDartCD         = mod:NewCDTimer(7, 305367, nil, mod:IsHealer() or mod:IsTank())
local timerHandCD         = mod:NewCDTimer(15, 305345)
local timerMarkCD         = mod:NewCDTimer(33, 305351)
local timerMeditation     = mod:NewTargetTimer(25, 305354)

local berserkTimer        = mod:NewBerserkTimer(600)

mod:AddBoolOption("HealthFrame", true)
mod:AddBoolOption("ShowMinSeedHP", false)
mod:AddBoolOption("ShowEverySeedHP", true)
mod:AddBoolOption("RangeFrame", true)
mod:AddBoolOption("AlwaysShowMediation", true)
mod:AddSetIconOption("SetIconOnSacrifice", 30115 , {8}, true)

local seedsHP = {}
local targets = {false,false,false}

local function minT()
    local m = 45000
    for i,v in pairs(seedsHP) do
        m = m < v and m or v
    end
    return math.max(1, math.floor((m or 0) / 45000 * 100))
end

local function seed1() return math.max(1, math.floor((seedsHP[targets[1] or ""] or 0) / 45000 * 100)) end
local function seed2() return math.max(1, math.floor((seedsHP[targets[2] or ""] or 0) / 45000 * 100)) end
local function seed3() return math.max(1, math.floor((seedsHP[targets[3] or ""] or 0) / 45000 * 100)) end

do
    local frame = CreateFrame("Frame")
    local damage
    frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
    frame:SetScript("OnEvent", function(self, event, timestamp, subEvent, sourceGUID, sourceName, sourceFlags, destGUID, destName, destFlags, ...)
        if seedsHP[destGUID] and seedsHP[destGUID] > 0 then
            damage = nil
            if subEvent == "SWING_DAMAGE" then
                damage = ...
            elseif subEvent == "SPELL_DAMAGE" or subEvent == "SPELL_PERIODIC_DAMAGE" then
                damage = select(4,...)
            end
            if damage then
                seedsHP[destGUID] = seedsHP[destGUID] - damage
            end
        end
    end)
end

function mod:OnCombatStart(delay)
    if self:IsDifficulty("normal10") then
        berserkTimer:Start()
        timerSacrificeCD:Start(30)
        warningSacSoon:Schedule(25)
    elseif self:IsDifficulty("heroic10") then
        timerSeedCD:Start()
        timerDartCD:Start()
        timerHandCD:Start()
        timerMarkCD:Start()
        targets = {false,false,false}
        if self.Options.ShowMinSeedHP then
            DBM.BossHealth:AddBoss(minT, L.Seed)
        end
    end
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(30066) then
        warningImpSoon:Cancel()
        warningImp:Show()
        DBM.BossHealth:AddBoss(17229, L.Kilrek)
    end
end

function mod:SacrificeTarget()
    local target = self:GetBossTarget(15688)
    if not target then return end
    if self.Options.SetIconOnSacrifice then
        self:SetIcon(target, 8)
    end
    if target == UnitName("player") then
        specWarnSacrifice:Show()
    end
end

function mod:HandTarget()
    local target = self:GetBossTarget(15688)
    if not target then return end
    if target == UnitName("player") then 
        self:PlaySound("fireinthe")
    end
    warnHand:Show(target)
end

function mod:MarkTarget()
    local target = self:GetBossTarget(15688)
    if not target then return end
    if target == UnitName("player") then
        self:PlaySound("t_zakazali")
    end
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(30115) then
        self:ScheduleMethod(0.1, "SacrificeTarget")
    elseif args:IsSpellID(305360) then
        timerSeedCD:Start()
    elseif args:IsSpellID(305367) then
        timerDartCD:Start()
    elseif args:IsSpellID(305345) then
        timerHandCD:Start()
        self:ScheduleMethod(0.1, "HandTarget")
    elseif args:IsSpellID(305351) then
        timerMarkCD:Start()
        self:ScheduleMethod(0.1, "MarkTarget")
    elseif args:IsSpellID(305354) then
        if args:IsPlayerSource() then
            self:PlaySound("dosvidania")
        end
    end
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(30115) then
        DBM.BossHealth:AddBoss(17248, L.DChains)
        warningSacrifice:Show(args.destName)
        timerSacrifice:Start(args.destName)
        timerSacrificeCD:Start()
        warningSacSoon:Cancel()
        warningSacSoon:Schedule(37)
        if args:IsPlayer() then
            self:PlaySound("arestovan", "ban4", "ban5", "dosvidania")
        end
    elseif args:IsSpellID(30065) then
        warningWeakened:Show(args.destName)
        timerWeakened:Start()
        warningImpSoon:Schedule(26)
    elseif args:IsSpellID(305351) then
        warnMark:Show(args.destName)
        if self.Options.AlwaysShowMediation or args:IsPlayer() then
            timerMeditation:Start(args.destName)
        end
        if args:IsPlayer() then
            specWarnMark:Show()
            yellMark:Yell()
            if self.Options.RangeFrame then
                DBM.RangeCheck:Show(10)
            end
        end
        if not self:AntiSpam(0.1, "tolik") and self:AntiSpam(5, "tolik2") then
            self:PlaySound("tolik", "s_sdelal", "kaktak")
        end
    elseif args:IsSpellID(305354) then
        if args:IsPlayer() then
            self:PlaySound("idisuda", "zahuyaru", "rubl")
        end
    elseif args:IsSpellID(305367) then
        warnDart:Show(args.spellName, args.destName, 1)
        if args:IsPlayer() and ((args.amount or 1) >= 7) then
            specWarnDart:Show(args.amount)
        end
    elseif args:IsSpellID(305360) then
        warnSeed:Show(args.destName)
        if args:IsPlayer() then 
            specWarnSeed:Show()
        end
        seedsHP[args.destGUID] = 45000
        if self.Options.ShowEverySeedHP then
            if not targets[1] then
                targets[1] = args.destGUID
                DBM.BossHealth:RemoveBoss(seed1)
                DBM.BossHealth:AddBoss(seed1, args.destName)
            elseif not targets[2] then
                targets[2] = args.destGUID
                DBM.BossHealth:RemoveBoss(seed2)
                DBM.BossHealth:AddBoss(seed2, args.destName)
            elseif not targets[3] then
                targets[3] = args.destGUID
                DBM.BossHealth:RemoveBoss(seed3)
                DBM.BossHealth:AddBoss(seed3, args.destName)
            end
        end
    end
end

function mod:SPELL_AURA_APPLIED_DOSE(args)
    if args:IsSpellID(305367) then
        warnDart:Show(args.spellName, args.destName, args.amount)
    end
end

function mod:SPELL_AURA_REMOVED(args)
    if args:IsSpellID(30115) then
        timerSacrifice:Cancel(args.destName)
        if self.Options.SetIconOnSacrifice then
            self:SetIcon(args.destName, 0)
        end
    elseif args:IsSpellID(305351) then
        if self.Options.RangeFrame then
            DBM.RangeCheck:Hide()
        end
    elseif args:IsSpellID(305360) then
        seedsHP[args.destGUID] = nil
        if self.Options.ShowEverySeedHP then
            if targets[1] == args.destGUID then
                targets[1] = false
                DBM.BossHealth:RemoveBoss(seed1)
            elseif targets[2] == args.destGUID then
                targets[2] = false
                DBM.BossHealth:RemoveBoss(seed2)
            elseif targets[3] == args.destGUID then
                targets[3] = false
                DBM.BossHealth:RemoveBoss(seed3)
            end 
        end
    end
end

-- function mod:CHAT_MSG_MONSTER_YELL(msg)
    -- if msg == L.YellKill then
        -- self:PlaySound("kozel")
    -- end
-- end

function mod:UNIT_DIED(args)
    local cid = self:GetCIDFromGUID(args.destGUID)
    if cid == 17229 then                --Kil'rek
        DBM.BossHealth:RemoveBoss(cid)
    elseif cid == 17248 then            --Demon Chains
        DBM.BossHealth:RemoveBoss(cid)
    elseif cid == 15688 then
        self:PlaySound("kozel")
    end
end
