local mod = DBM:NewMod("Zluker", "DBM-Karazhan")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4500 $"):sub(12, -3))
mod:SetCreatureID(35015, 35016)
mod:RegisterCombat("yell", L.YellZluker)

--35016(boss1) - Эльфира
--35015(boss2) - Галиндра

mod:RegisterEvents(
    "SPELL_CAST_START",
    "SPELL_CAST_SUCCESS",
    "CHAT_MSG_MONSTER_YELL",
    "SPELL_DAMAGE"
)

local warnMagicSoon            = mod:NewSoonAnnounce(305535, 4)
local warnDefyGravity          = mod:NewTargetAnnounce(305537, 3)
local warnSummonSoon           = mod:NewSoonAnnounce(305540, 2)

local specWarnMagic            = mod:NewSpecialWarningSpell(305535)
local specWarnDefyGravity      = mod:NewSpecialWarningMove(305537)
local specWarnShining          = mod:NewSpecialWarningMove(305533)

local yellGravity              = mod:NewYell(305537)

local timerMagicCD             = mod:NewCDTimer(48, 305535)
local timerSummonCD            = mod:NewCDTimer(70, 305540)
local timerCombatStart         = mod:NewTimer(80, "TimerCombatStart", 2457)

function mod:GravityTarget()
    warnDefyGravity:Show(UnitName("boss1target"))
    if UnitName("boss1target") == UnitName("player") then
        self:PlaySound("sunstrike")
        specWarnDefyGravity:Show()
        yellGravity:Yell()
    end
end

function mod:ShiningTarget()
    if UnitName("boss2target") == UnitName("player") then
        specWarnShining:Show()
    end
end

function mod:OnCombatStart(delay)
    warnMagicSoon:Schedule(40)
    warnSummonSoon:Schedule(65)
    timerMagicCD:Start(45-delay)
    timerSummonCD:Start(-delay)
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(305535) then
        self:PlaySound("djeep", "resist")
        warnMagicSoon:Schedule(43)
        specWarnMagic:Show()
        timerMagicCD:Start()
    elseif args:IsSpellID(305537) then
        self:ScheduleMethod(0.1, "GravityTarget")
    elseif args:IsSpellID(305533) then
        self:ScheduleMethod(0.1, "ShiningTarget")
    end
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(305540) then
        warnSummonSoon:Schedule(65)
        timerSummonCD:Start()
    end
end

function mod:SPELL_DAMAGE(args)
    if args:IsSpellID(305535) and args:IsPlayer() and args.overkill > 0 then
        self:PlaySound("fiasco")
    end
end

function mod:CHAT_MSG_MONSTER_YELL(msg)
    if msg == L.YellPull then
        timerCombatStart:Start()
    end
end