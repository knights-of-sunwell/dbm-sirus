local mod = DBM:NewMod("Nightbane", "DBM-Karazhan")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4822 $"):sub(12, -3))
mod:SetCreatureID(17225)

--mod:RegisterCombat("yell", L.DBM_NB_YELL_PULL)
mod:RegisterCombat("combat")
mod:SetUsedIcons(6, 7, 8)

mod:RegisterEvents(
    "SPELL_CAST_START",
    "SPELL_CAST_SUCCESS",
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_REMOVED",
    "CHAT_MSG_MONSTER_EMOTE",
    "CHAT_MSG_MONSTER_YELL",
    "UNIT_HEALTH"
)

local warnBone               = mod:NewSpellAnnounce(37098, 3)
local warnFear               = mod:NewSpellAnnounce(39427, 2)
local warnBreath             = mod:NewSpellAnnounce(30210, 4)
local warnTailSwipe          = mod:NewSpellAnnounce(25653, 1)
local warnAsh                = mod:NewTargetAnnounce(30130, 2)
local warnAirPhase           = mod:NewAnnounce("WarnAirPhase", 2, "Interface\\AddOns\\DBM-Core\\textures\\CryptFiendBurrow.blp")

local specWarnCharred        = mod:NewSpecialWarningMove(30129)
mod:AddAnnounceLine(DBM_HEROIC_MODE)
local warnGrievingFire       = mod:NewSpellAnnounce(305375, 1)
local warnConfl              = mod:NewSpellAnnounce(305377, 2)
local warnPyromancer         = mod:NewTargetAnnounce(305382, 3)
local warnCall               = mod:NewSpellAnnounce(305386, 4)

local specWarnPyromancer     = mod:NewSpecialWarningYou(305382)

local timerNightbane         = mod:NewTimer(27, "timerNightbane", "Interface\\Icons\\Ability_Mount_Undeadhorse")
local timerAirPhase          = mod:NewTimer(40, "timerAirPhase", "Interface\\AddOns\\DBM-Core\\textures\\CryptFiendBurrow.blp")
local timerFearCD            = mod:NewCDTimer(30, 39427)
local timerBreathCD          = mod:NewCDTimer(17, 30210)
local timerTailSwipeCD       = mod:NewCDTimer(15, 25653)
mod:AddTimerLine(DBM_HEROIC_MODE)
local timerGrievingFireCD    = mod:NewCDTimer(13, 305375)
local timerConflCD           = mod:NewCDTimer(30, 305377)
local timerPyromancerCD      = mod:NewCDTimer(62, 305382)
local timerCallCD            = mod:NewCDTimer(125, 305386)
local timerDarkWaveCD        = mod:NewCDTimer(20, 305392)

mod:AddBoolOption("RemoveWeaponOnMindControl", true)
mod:AddBoolOption("RemoveDefBuffsOnMindControl", true)
mod:AddSetIconOption("SetIconOnPyromancer", 305382, {8, 7, 6}, true)

local pyromancerTargets = {}
local alivePyromancers = 0
local groundPhase = 0
local isPyroFirst = true
local isStart = true

function mod:OnCombatStart(delay)
    if mod:IsDifficulty("normal10") then
        timerFearCD:Start(-delay)
        timerBreathCD:Start(-delay)
        timerTailSwipeCD:Start(-delay)
    elseif mod:IsDifficulty("heroic10") and isStart then
        timerGrievingFireCD:Start(-delay)
        timerConflCD:Start(-delay)
        timerPyromancerCD:Start(-delay)
        timerCallCD:Start(-delay)
        table.wipe(pyromancerTargets)
        groundPhase = 0
        alivePyromancers = 0
        isPyroFirst = true
        isStart = false
    end
end

function mod:RemoveWeapon()
    if self:IsWeaponDependent() then
        PickupInventoryItem(16)
        PutItemInBackpack()
        PickupInventoryItem(17)
        PutItemInBackpack()
    elseif select(2, UnitClass("player")) == "HUNTER" then
        PickupInventoryItem(18)
        PutItemInBackpack()
    end
end

function mod:RemoveBuffs()
    CancelUnitBuff("player", (GetSpellInfo(48469)))        -- Mark of the Wild
    CancelUnitBuff("player", (GetSpellInfo(48470)))        -- Gift of the Wild
    CancelUnitBuff("player", (GetSpellInfo(48169)))        -- Shadow Protection
    CancelUnitBuff("player", (GetSpellInfo(48170)))        -- Prayer of Shadow Protection
    CancelUnitBuff("player", (GetSpellInfo(48707)))        -- Anti-Magic Shell
    CancelUnitBuff("player", (GetSpellInfo(49222)))        -- Bone Shield
    CancelUnitBuff("player", (GetSpellInfo(48792)))        -- Icebound Fortitude
    CancelUnitBuff("player", (GetSpellInfo(304822)))       -- Icebound Fortitude (T4)
    CancelUnitBuff("player", (GetSpellInfo(23920)))        -- Spell Reflection
    CancelUnitBuff("player", (GetSpellInfo(871)))          -- Shield Wall
    CancelUnitBuff("player", (GetSpellInfo(304687)))       -- Shield Wall (T4)
    CancelUnitBuff("player", (GetSpellInfo(642)))          -- Divine Shield
    CancelUnitBuff("player", (GetSpellInfo(498)))          -- Divine Protection
    CancelUnitBuff("player", (GetSpellInfo(64205)))        -- Divine Sacrifice
    CancelUnitBuff("player", (GetSpellInfo(47585)))        -- Dispersion
    CancelUnitBuff("player", (GetSpellInfo(19263)))        -- Deterrence
    CancelUnitBuff("player", (GetSpellInfo(25228)))        -- Soul Link
    CancelUnitBuff("player", (GetSpellInfo(47891)))        -- Shadow Ward
    CancelUnitBuff("player", (GetSpellInfo(45438)))        -- Ice Block
    CancelUnitBuff("player", (GetSpellInfo(43012)))        -- Frost Ward
    CancelUnitBuff("player", (GetSpellInfo(43010)))        -- Fire Ward
    CancelUnitBuff("player", (GetSpellInfo(43020)))        -- Mana Shield
    CancelUnitBuff("player", (GetSpellInfo(43039)))        -- Ice Barrier
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(39427) then
        warnFear:Show()
        timerFearCD:Start()
    elseif args:IsSpellID(30210) then
        warnBreath:Start()
        timerBreathCD:Start()
    elseif args:IsSpellID(305375) then
        self:ScheduleMethod(2, "PlaySound", "oh")
        warnGrievingFire:Show()
        timerGrievingFireCD:Start()
    elseif args:IsSpellID(305377) then
        self:PlaySound("orgasm", "AAAAA", "AAAA_lew")             -- AAAAAAAAAA!!!
        warnConfl:Show()
        timerConflCD:Start()
    elseif args:IsSpellID(305386) then
        warnCall:Show()
        timerGrievingFireCD:Cancel()
        timerConflCD:Cancel()
        timerDarkWaveCD:Start()
        if UnitAura("player", GetSpellInfo(305382), nil, "HARMFUL") or UnitAura("player", GetSpellInfo(305384), nil, "HARMFUL") then
            if self.Options.RemoveWeaponOnMindControl then
                self:ScheduleMethod(1.5, "RemoveWeapon")
            end
            if self.Options.RemoveDefBuffsOnMindControl then
                self:ScheduleMethod(1.8, "RemoveBuffs")
            end
        end
        table.wipe(pyromancerTargets)
        isPyroFirst = true
        groundPhase = 0
        alivePyromancers = 0
    end
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(37098) then
        warnBone:Show()
    elseif args:IsSpellID(25653) then
        warnTailSwipe:Start()
        timerTailSwipeCD:Start()
    elseif args:IsSpellID(305392) then
        timerDarkWaveCD:Start()
    end
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(30129) and args:IsPlayer() then
        specWarnCharred:Show()
    elseif args:IsSpellID(30130) then
        warnAsh:Show(args.destName)
    elseif args:IsSpellID(305382) then
        if args:IsPlayer() then
            self:PlaySound("welcome")    -- Welcome to the club buddy... *fucking slap*
            specWarnPyromancer:Show()
        end
        pyromancerTargets[#pyromancerTargets + 1] = args.destName
        if #pyromancerTargets >= 3 and isPyroFirst then
            warnPyromancer:Show(table.concat(pyromancerTargets, "<, >"))
            if self.Options.SetIconOnPyromancer then
                table.sort(pyromancerTargets, function(v1,v2) return DBM:GetRaidSubgroup(v1) < DBM:GetRaidSubgroup(v2) end)
                local pyroIcons = 8
                for i, v in ipairs(pyromancerTargets) do
                    self:SetIcon(v, pyroIcons)
                    pyroIcons = pyroIcons - 1
                end
            end
            isPyroFirst = false
        end
    elseif args:IsSpellID(305388) then
        alivePyromancers = alivePyromancers + 1
    end
end

function mod:SPELL_AURA_REMOVED(args)
    if args:IsSpellID(305388) then
        self:RemoveIcon(args.destName)
        if groundPhase == alivePyromancers - 1 then
            timerDarkWaveCD:Cancel()
            timerGrievingFireCD:Start(35)
            timerConflCD:Start(52)
            timerPyromancerCD:Start(84)
            timerCallCD:Start(147)
        else 
            groundPhase = groundPhase + 1
        end
    end
end

function mod:CHAT_MSG_MONSTER_YELL(msg)
    if msg == L.YellAir then
        warnAirPhase:Show()
        timerAirPhase:Start()
        timerFearCD:Cancel()
        timerBreathCD:Cancel()
        timerTailSwipeCD:Cancel()
        timerFearCD:Schedule(40)
        timerBreathCD:Schedule(40)
        timerTailSwipeCD:Schedule(40)
    end
end

function mod:CHAT_MSG_MONSTER_EMOTE(msg)
     if msg == L.EmoteStart then
        isStart = true
        timerNightbane:Start()
        self:PlaySound("pike")
     end
 end
