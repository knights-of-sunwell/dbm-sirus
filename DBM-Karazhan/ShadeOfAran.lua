local mod = DBM:NewMod("Aran", "DBM-Karazhan")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4800 $"):sub(12, -3))
mod:SetCreatureID(16524)
mod:RegisterCombat("combat")
mod:SetUsedIcons(7, 8)

mod:RegisterEvents(
    "SPELL_CAST_START",
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_REMOVED",
    "SPELL_SUMMON",
    "SPELL_PERIODIC_DAMAGE"
)

local warningFlameCast      = mod:NewCastAnnounce(30004, 4)
local warningArcaneCast     = mod:NewCastAnnounce(29973, 4)
local warningBlizzard       = mod:NewSpellAnnounce(29969, 3)
local warningElementals     = mod:NewSpellAnnounce(37053, 3)
local warningChains         = mod:NewTargetAnnounce(29991, 2)
local warningFlameTargets   = mod:NewTargetAnnounce(29946, 4)

local specWarnDontMove      = mod:NewSpecialWarning("DBM_ARAN_DO_NOT_MOVE")
local specWarnArcane        = mod:NewSpecialWarningRun(29973)
local specWarnBlizzard      = mod:NewSpecialWarningMove(29951)

mod:AddAnnounceLine(DBM_HEROIC_MODE)
local warnFrostbite         = mod:NewSpellAnnounce(305324, 2)
local warnFreeze            = mod:NewTargetAnnounce(305326, 4)
local warnInfernal          = mod:NewTargetAnnounce(305336, 2)
local warnWinter            = mod:NewSpellAnnounce(305329, 3)
local warnFamilliars        = mod:NewSpellAnnounce(305331, 3)
local warnFlameWreath       = mod:NewSpellAnnounce(305338, 3)

local specWarnFreeze        = mod:NewSpecialWarning("SpecWarnFreeze")
local specWarnInfernal      = mod:NewSpecialWarningMove(305336)

local yellFreeze            = mod:NewYell(305326)
local yellInfernal          = mod:NewYell(305336, "YELL")

local timerSpecial          = mod:NewTimer(30, "TimerSpecial", "Interface\\Icons\\INV_Enchant_EssenceMagicLarge")
local timerFlameCast        = mod:NewCastTimer(5, 30004)
local timerArcaneExplosion  = mod:NewCastTimer(10, 29973)
local timerBlizzadCast      = mod:NewCastTimer(3.7, 29969)
local timerFlame            = mod:NewBuffActiveTimer(20, 29946)
local timerBlizzad          = mod:NewBuffActiveTimer(40, 29951)
local timerElementals       = mod:NewBuffActiveTimer(90, 37053)
local timerChains           = mod:NewTargetTimer(10, 29991)
local berserkTimer          = mod:NewBerserkTimer(900)
mod:AddTimerLine(DBM_HEROIC_MODE)
local timerSpecialH         = mod:NewTimer(50, "TimerSpecialH", "Interface\\Icons\\ability_mage_GreaterInvisibility")
local timerFreeze           = mod:NewTargetTimer(30, 305328)
local timerFlameH           = mod:NewBuffActiveTimer(10, 305338)

mod:AddSetIconOption("SetIconOnFreeze", 305326 , {8}, true)
mod:AddSetIconOption("SetIconOnInfernal", 305336 , {7}, true)

local WreathTargets = {}

function mod:warnFlameWreathTargets()
    warningFlameTargets:Show(table.concat(WreathTargets, "<, >"))
    table.wipe(WreathTargets)
end

function mod:warnFreezeTarget()
    local target = self:GetBossTarget(16524)
    if not target then return end
    warnFreeze:Show(target)
    if self.Options.SetIconOnFreeze then
        self:SetIcon(target, 8)
    end
    if target == UnitName("player") then
        specWarnFreeze:Show()
        yellFreeze:Yell()
        self:PlaySound("surprise")
    end
end

function mod:warnInfernalTarget()
    local target = self:GetBossTarget(16524)
    if not target then return end
    warnInfernal:Show(target)
    if self.Options.SetIconOnInfernal then
        self:SetIcon(target, 7, 5)
    end
    if target == UnitName("player") then
        specWarnInfernal:Show()
        yellInfernal:Yell()
        self:PlaySound("sunstrike")
    end
end

function mod:OnCombatStart(delay)
    if self:IsDifficulty("normal10") then
        berserkTimer:Start(-delay)
        timerSpecial:Start()
        table.wipe(WreathTargets)
    elseif self:IsDifficulty("heroic10") then
        timerSpecialH:Start()
    end
end

local pullTime = GetTime()
local f = CreateFrame("Frame", nil, UIParent)
f:RegisterEvent("PLAYER_REGEN_DISABLED")
f:RegisterEvent("PLAYER_REGEN_ENABLED")
f:SetScript("OnEvent", function(self, event)
    if event == "PLAYER_REGEN_DISABLED" and mod:IsDifficulty("heroic10") then
        for i = 1, GetNumRaidMembers() do
            local uId  = "raid" .. i .. "target"
            local guid = UnitGUID(uId)
            if guid and (bit.band(guid:sub(1, 5), 0x00F) == 3 or bit.band(guid:sub(1, 5), 0x00F) == 5) then
                local cId = tonumber(guid:sub(9, 12), 16)
                if cId == 16524 and UnitAffectingCombat(uId) then
                    pullTime = GetTime()
                    break
                end
            end
        end
    elseif event == "PLAYER_REGEN_ENABLED" and mod:IsDifficulty("heroic10") and GetTime() - pullTime < 3 then
        DBM:CreatePizzaTimer(32, L.TimerBossRespawn, true)
    end
end)

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(30004) then
        self:PlaySound("stop")
        warningFlameCast:Show()
        timerFlameCast:Start()
        timerFlame:Schedule(5)
        timerSpecial:Start()
    elseif args:IsSpellID(29973) then
        self:PlaySound("sunstrike")
        warningArcaneCast:Show()
        specWarnArcane:Show()
        timerArcaneExplosion:Start()
        timerSpecial:Start()
    elseif args:IsSpellID(29969) then
        warningBlizzard:Show()
        timerBlizzadCast:Start()
        timerBlizzad:Schedule(3.7)
        timerSpecial:Start()
    elseif args:IsSpellID(305324) then
        warnFrostbite:Show()
    elseif args:IsSpellID(305326) then
        self:ScheduleMethod(0.1, "warnFreezeTarget")
    elseif args:IsSpellID(305336, 305337) then
        self:ScheduleMethod(0.1, "warnInfernalTarget")
    elseif args:IsSpellID(305329) then
        self:PlaySound("primo")
        warnWinter:Show()
        timerSpecialH:Start()
    elseif args:IsSpellID(305331) then
        self:PlaySound("tobecon","dramatic")
        warnFamilliars:Show()
    elseif args:IsSpellID(305338) then
        self:PlaySound("stop")
        warnFlameWreath:Show()
        timerFlameCast:Start()
        timerFlameH:Schedule(5)
        timerSpecialH:Start()
    end
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(29991) then
        warningChains:Show(args.destName)
        timerChains:Start(args.destName)
    elseif args:IsSpellID(29946, 305330) then
        WreathTargets[#WreathTargets + 1] = args.destName
        if args:IsPlayer() then
            specWarnDontMove:Show()
        end
        self:UnscheduleMethod("warnFlameWreathTargets")
        self:ScheduleMethod(0.3, "warnFlameWreathTargets")
    elseif args:IsSpellID(305328) then
        timerFreeze:Start(args.destName)
    end
end

function mod:SPELL_AURA_REMOVED(args)
    if args:IsSpellID(29991) then
        timerChains:Cancel(args.destName)
    elseif args:IsSpellID(305328) then
        timerFreeze:Cancel(args.destName)
        self:RemoveIcon(args.destName)
    elseif args:IsSpellID(305333) then
        timerSpecialH:Start()
    end
end


function mod:SPELL_SUMMON(args)
    if args:IsSpellID(29962, 37051, 37052, 37053) and self:AntiSpam(10, "summon") then
        warningElementals:Show()
        timerElementals:Start()
    end
end

function mod:SPELL_PERIODIC_DAMAGE(args)
    if args:IsSpellID(29951) and args:IsPlayer() and self:AntiSpam(3, "blizzard") then
        specWarnBlizzard:Show()
    end
end