local mod = DBM:NewMod("Malacrass", "DBM-ZulAman")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4856 $"):sub(12, -3))

mod:SetCreatureID(24239, 24240, 24241, 24242, 24243, 24244, 24245, 24246, 24247)
mod:RegisterCombat("yell", L.YellPull)
mod:RegisterKill("yell", L.YellKill)

-- 24240, //Alyson Antille
-- 24241, //Thurg
-- 24242, //Slither
-- 24243, //Lord Raadan
-- 24244, //Gazakroth
-- 24245, //Fenstalker
-- 24246, //Darkheart
-- 24247  //Koragg

mod:RegisterEvents(
    "SPELL_CAST_START",
    "SPELL_CAST_SUCCESS",
    "SPELL_AURA_APPLIED"
)

local warnSiphon            = mod:NewAnnounce("WarnSiphon", 2, 43501)
local warnSpecial           = mod:NewAnnounce("WarnSpecial", 3)
local warnSpecialOn         = mod:NewAnnounce("WarnSpecialOn", 3)

local specWarnMove          = mod:NewSpecialWarning("SpecWarnMove")

local timerBoltsCD          = mod:NewCDTimer(40, 43383)
local timerSpecial          = mod:NewTimer(9,"TimerSpecial", 43501)

local class, classEN = "",""
local spellSounds = {[43429] = "surprise", [43440] = "rain", [43442] = "othodi", [305658] = "begi"}

function mod:OnCombatStart(delay)
    timerBoltsCD:Start(20-delay)
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(--[[DR]]
                      --[[HU]]
                      --[[MG]]41383,43428,
                      --[[PA]]43451,
                      --[[PR]]41372,41374,
                      --[[RO]]
                      --[[SH]]43435,43548
                      --[[WL]]
                      --[[WR]]
                      --[[DK]]) then
        warnSpecial:Show(args.spellName, args.spellId)
        timerSpecial:Start(class, classEN)
    end
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(43383) then
        timerBoltsCD:Cancel()
        timerBoltsCD:Schedule(40.2, 12.8) -- backup if timer delays by 13s
        timerBoltsCD:Start()
    elseif args:IsSpellID(--[[DR]]43420,44131,43501,
                          --[[HU]]43444,43447,43449,
                          --[[MG]]43426,43427,
                          --[[PA]]43429,43430,
                          --[[PR]]41375,43432,43550,44416,
                          --[[RO]]43433,43457,43461,
                          --[[SH]]43436,
                          --[[WL]]43439,43440,43522,
                          --[[WR]]43441,43442,43443,
                          --[[DK]]305658,305659,305660) then
        if args:IsDestTypePlayer() and args.destName then
            warnSpecialOn:Show(args.spellName, args.destName, args.spellId)
        else
            warnSpecial:Show(args.spellName, args.spellId)
        end
        timerSpecial:Start(class, classEN)
    end
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(43501) then
        class, classEN = UnitClass(args.destName)
        warnSiphon:Show(args.destName, classEN)
        timerSpecial:Start(class, classEN)
    elseif args:IsSpellID(43429, 43440, 43442, 305658) then -- AOEs
        if args:IsPlayer() then
            specWarnMove:Show(args.spellName)
            self:PlaySound(spellSounds[args.spellId])
        end
    end
end

-- from https://github.com/TrinityCore/TrinityCore/blob/3.3.5/src/server/scripts/EasternKingdoms/ZulAman/boss_hexlord.cpp

-- SPELL_SPIRIT_BOLTS            = 43383,
-- SPELL_DRAIN_POWER             = 44131,
-- SPELL_SIPHON_SOUL             = 43501,

-- // Druid
-- SPELL_DR_THORNS               = 43420,
-- SPELL_DR_LIFEBLOOM            = 43421,
-- SPELL_DR_MOONFIRE             = 43545,

-- // Hunter
-- SPELL_HU_EXPLOSIVE_TRAP       = 43444,
-- SPELL_HU_FREEZING_TRAP        = 43447,
-- SPELL_HU_SNAKE_TRAP           = 43449,

-- // Mage
-- SPELL_MG_FIREBALL             = 41383,
-- SPELL_MG_FROST_NOVA           = 43426,
-- SPELL_MG_ICE_LANCE            = 43427,
-- SPELL_MG_FROSTBOLT            = 43428,

-- // Paladin
-- SPELL_PA_CONSECRATION         = 43429,
-- SPELL_PA_AVENGING_WRATH       = 43430,
-- SPELL_PA_HOLY_LIGHT           = 43451,

-- // Priest
-- SPELL_PR_HEAL                 = 41372,
-- SPELL_PR_MIND_BLAST           = 41374,
-- SPELL_PR_SW_DEATH             = 41375,
-- SPELL_PR_PSYCHIC_SCREAM       = 43432,
-- SPELL_PR_MIND_CONTROL         = 43550,
-- SPELL_PR_PAIN_SUPP            = 44416,

-- // Rogue
-- SPELL_RO_BLIND                = 43433,
-- SPELL_RO_SLICE_DICE           = 43457,
-- SPELL_RO_WOUND_POISON         = 43461,

-- // Shaman
-- SPELL_SH_CHAIN_LIGHT          = 43435,
-- SPELL_SH_FIRE_NOVA            = 43436,
-- SPELL_SH_HEALING_WAVE         = 43548,

-- // Warlock
-- SPELL_WL_CURSE_OF_DOOM        = 43439,
-- SPELL_WL_RAIN_OF_FIRE         = 43440,
-- SPELL_WL_UNSTABLE_AFFL        = 43522,
-- SPELL_WL_UNSTABLE_AFFL_DISPEL = 43523,

-- // Warrior
-- SPELL_WR_MORTAL_STRIKE        = 43441,
-- SPELL_WR_WHIRLWIND            = 43442,
-- SPELL_WR_SPELL_REFLECT        = 43443,

-- // Thurg
-- SPELL_BLOODLUST               = 43578,
-- SPELL_CLEAVE                  = 15496,

-- // Gazakroth
-- SPELL_FIREBOLT                = 43584,

-- // Alyson Antille
-- SPELL_FLASH_HEAL              = 43575,
-- SPELL_DISPEL_MAGIC            = 43577,

-- // Lord Raadan
-- SPELL_FLAME_BREATH            = 43582,
-- SPELL_THUNDERCLAP             = 43583,

-- // Darkheart
-- SPELL_PSYCHIC_WAIL            = 43590,

-- // Slither
-- SPELL_VENOM_SPIT              = 43579,

-- // Fenstalker
-- SPELL_VOLATILE_INFECTION      = 43586,

-- // Koragg
-- SPELL_COLD_STARE              = 43593,
-- SPELL_MIGHTY_BLOW             = 43592