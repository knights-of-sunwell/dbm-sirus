local mod = DBM:NewMod("Janalai", "DBM-ZulAman")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4873 $"):sub(12, -3))

mod:SetCreatureID(23578)
mod:RegisterCombat("yell", L.YellPull)
mod:RegisterKill("yell", L.YellKill)

mod:RegisterEvents(
    "SPELL_CAST_START",
    "SPELL_AURA_APPLIED_DOSE",
    "CHAT_MSG_MONSTER_YELL",
    "SPELL_DAMAGE",
    "SPELL_MISSED",
    "UNIT_DIED"
)

local warnHatchersSoon      = mod:NewAnnounce("WarnHatchersSoon", 1, 26297)
local warnHatchers          = mod:NewAnnounce("WarnHatchers", 2, 26297)
local warnBombs             = mod:NewSpellAnnounce(42621, 3)
local warnFlameBuffet       = mod:NewAnnounce("WarnFlameBuffet", 4, 43299)

local timerHatchers         = mod:NewTimer(155, "TimerHatchers", 26297)
local timerBombsCD          = mod:NewCDTimer(40, 42621)
local timerExplosion        = mod:NewTimer(11, "TimerExplosion", 66313)
local timerBreathCD         = mod:NewCDTimer(12, 43140)
local timerAddsKill         = mod:NewAchievementTimer(10, 6137)

mod:AddBoolOption("AchievementCheckBomb", true, "announce")
mod:AddBoolOption("AchievementCheckAdds", true, "announce")

local bombFail = true
local bombTargets = {}
local addsKilled = 0

function mod:OnCombatStart(delay)
    warnHatchersSoon:Schedule(9-delay)
    timerHatchers:Start(14-delay)
    timerBombsCD:Start(40-delay)
    timerBreathCD:Start(-delay)
    bombFail = true
    table.wipe(bombTargets)
    addsKilled = 0
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(43140) then
        timerBreathCD:Start()
    end
end

function mod:BombFail()
    SendChatMessage(L.BombFail:format(GetAchievementLink(6139) , table.concat(bombTargets, ", ")), "RAID_WARNING")
    bombFail = false
    table.wipe(bombTargets)
end

function mod:SPELL_DAMAGE(args)
    if args:IsSpellID(42630) and args:IsDestTypePlayer() and bombFail and self.Options.AchievementCheckBomb and DBM:GetRaidRank() > 0 then
        bombTargets[#bombTargets + 1] = args.destName
        self:UnscheduleMethod("BombFail")
        self:ScheduleMethod(0.1, "BombFail")
    end
end

function mod:SPELL_MISSED(args)
    if args:IsSpellID(42630) and args:IsDestTypePlayer() and args.absorbed > 0 and bombFail and self.Options.AchievementCheckBomb and DBM:GetRaidRank() > 0 then
        bombTargets[#bombTargets + 1] = args.destName
        self:UnscheduleMethod("BombFail")
        self:ScheduleMethod(0.1, "BombFail")
    end
end

function mod:SPELL_AURA_APPLIED_DOSE(args)
    if args:IsSpellID(43299) then
        if args.amount >= 10 and self:AntiSpam(3, "buffet") then
            warnFlameBuffet:Show(args.spellName, args.destName, args.amount)
        end
    end
end

function mod:CHAT_MSG_MONSTER_YELL(msg)
    if msg == L.YellBombs then
        self:PlaySound("bomb_p", "bomba", "t_zaminirovali", "strashno")
        self:ScheduleMethod(10, "PlaySound", "wtf_boom" )
        warnBombs:Show()
        timerBreathCD:Extend(10)
        timerExplosion:Start()
        timerBombsCD:Start()
    elseif msg == L.YellHatcher then
        self:PlaySound("ispancy")
        warnHatchersSoon:Schedule(145)
        warnHatchers:Show()
        timerHatchers:Start()
    end
end

function mod:AddsCheck()
    if addsKilled < 40 then
        SendChatMessage(L.AddsFail:format(GetAchievementLink(6137), addsKilled), "RAID_WARNING")
    elseif addsKilled == 40 then
        SendChatMessage(L.AddsDone:format(GetAchievementLink(6137)), "RAID_WARNING")
    end
end

function mod:UNIT_DIED(args)
    local cid = self:GetCIDFromGUID(args.destGUID)
    if cid == 23598 then
        addsKilled = addsKilled + 1
        if addsKilled == 1 then
            if self.Options.AchievementCheckAdds and DBM:GetRaidRank() > 0 then
                self:ScheduleMethod(10, "AddsCheck")
            end
            timerAddsKill:Start()
        end
    end
end
